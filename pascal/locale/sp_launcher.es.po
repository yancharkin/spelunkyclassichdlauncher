msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"X-Generator: Poedit 1.8.11\n"

#: sp_launcher_unit1.textfullscreen
msgid "fullscreen"
msgstr "pantalla completa"

#: sp_launcher_unit1.textwindowed
msgid "windowed"
msgstr "ventana"

#: tform1.button1.caption
msgctxt "TFORM1.BUTTON1.CAPTION"
msgid "Save and exit"
msgstr "Guardar y salir"

#: tform1.button2.caption
msgctxt "TFORM1.BUTTON2.CAPTION"
msgid "Save and launch the game"
msgstr "Guardar y iniciar el juego"

#: tform1.caption
msgid "Spelunky Classic HD Launcher"
msgstr "Lanzador Spelunky Classic HD"

#: tform1.label1.caption
msgctxt "TFORM1.LABEL1.CAPTION"
msgid "Mode:"
msgstr "Modo:"

#: tform1.label2.caption
msgctxt "TFORM1.LABEL2.CAPTION"
msgid "Language:"
msgstr "Idioma:"
